#include "messageRemoverTask.h"
#include "main.h"
#include "stm32_CAN_SnifferMessage.h"
#include "FreeRTOS.h"
#include "event_groups.h"
#include "eventFlags.h"
//--------------------------------------------------------------------------------------------------
extern osMessageQueueId_t msgQueueHighCANId;
extern osMessageQueueId_t msgQueueDebugId;
extern osMessageQueueId_t msgQueueStorageId;
extern EventGroupHandle_t currentMessage;
extern EventGroupHandle_t nextMessage;
extern osEventFlagsId_t debugCanActiveEventFlagId;
//--------------------------------------------------------------------------------------------------
osThreadId_t messageRemoverTaskHandle;
const osThreadAttr_t messageRemoverTask_attributes = {
  .name = "messageRemoverTask",
  .priority = (osPriority_t) osPriorityHigh,
  .stack_size = 128 * 4
};
//--------------------------------------------------------------------------------------------------
/**
* @brief Task responsible for removing messages received by message listener tasks
* @param argument: Not used
* @retval None
*/
void messageRemoverTask(void *argument)
{
  CAN_SnifferMessage message;

  while(1)
  {
    xEventGroupSync(currentMessage, MSG_REMOVER_TASK_BIT, HIGH_MSG_SYNC_BITS, osWaitForever);

    osMessageQueueGet(msgQueueHighCANId, &message, 0U, 0);
    osMessageQueuePut(msgQueueStorageId, &message, 0U, 0);
    if (osEventFlagsGet(debugCanActiveEventFlagId) & UART_DEBUG_TASK_BIT)
    {
      // if message cannot be put into the queue in 10 ms, drop message
      osMessageQueuePut(msgQueueDebugId, &message, 0U, 10);
    }
    // if message cannot be put into the queue in 10 ms, drop message
    HAL_GPIO_TogglePin(GPIOK, GPIO_PIN_3);

    xEventGroupSync(nextMessage, MSG_REMOVER_TASK_BIT, HIGH_MSG_SYNC_BITS, osWaitForever);
  }
}
//--------------------------------------------------------------------------------------------------
