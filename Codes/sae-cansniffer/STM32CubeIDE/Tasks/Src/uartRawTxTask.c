#include "uartRawTxTask.h"
#include "main.h"
#include "stm32f4xx_hal_rtc.h"
#include "stm32_CAN_SnifferMessage.h"

#include "FreeRTOS.h"
#include "event_groups.h"
#include "eventFlags.h"
#include <stdbool.h>

extern RTC_HandleTypeDef hrtc;
extern UART_HandleTypeDef huart6;
extern osMessageQueueId_t msgQueueHighCANId;
extern osEventFlagsId_t uartEventFlagId;

extern EventGroupHandle_t currentMessage;
extern EventGroupHandle_t nextMessage;


/* Definitions for uartRawTx */
/*FIXME: Test thread priority mode (is this setting good?)*/
const osThreadAttr_t uartRawTx_attributes = {
  .name = "uartRawTx",
  .priority = (osPriority_t) osPriorityAboveNormal,
  .stack_size = 128 * 4
};

void uartRawTxTask(void *argument)
{
	CAN_SnifferMessage msgToSend;

	static bool active = false;

	while(1)
	{
		if (!active)
		{
			active = true;
			// Activate communication between CAN sniffer and radio TX
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1, GPIO_PIN_SET);
			osDelay(10U);
		}

		osMessageQueuePeek(msgQueueHighCANId, &msgToSend, 0U, osWaitForever);
		HAL_UART_Transmit(&huart6, (uint8_t*) &msgToSend, sizeof(msgToSend), osWaitForever);

		xEventGroupSync(currentMessage, RADIO_TASK_BIT, HIGH_MSG_SYNC_BITS, osWaitForever);
		xEventGroupSync(nextMessage, RADIO_TASK_BIT, HIGH_MSG_SYNC_BITS, osWaitForever);
	}
}
