#include <stdio.h>
#include "storageTask.h"
#include "FreeRTOS.h"
#include "main.h"
#include "stm32f4xx_hal_rtc.h"
#include "stm32_CAN_SnifferMessage.h"
#include "fatfs.h"
#include "ff.h"
#include "event_groups.h"
#include "eventFlags.h"
//--------------------------------------------------------------------------------------------------
#define f_unmount(path) f_mount(0, path, 0)
//--------------------------------------------------------------------------------------------------
#define MAX_MESSAGE_BUFFER_SIZE 50
//--------------------------------------------------------------------------------------------------
extern RTC_HandleTypeDef hrtc;
extern osMessageQueueId_t msgQueueStorageId;
//--------------------------------------------------------------------------------------------------
const osThreadAttr_t storageTask_attributes = {
  .name = "storageTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 512 * 4
};
//--------------------------------------------------------------------------------------------------
/**
* @brief Task responsible for storing sniffed messages
* @param argument: Not used
* @retval None
*/
void storageTask(void *argument)
{
  static uint32_t receivedMessages = 0;
  char filename[20];
  RTC_DateTypeDef dateStamp;
  FRESULT fr;
  UINT bw;
  const UINT bufferSize = MAX_MESSAGE_BUFFER_SIZE * sizeof(CAN_SnifferMessage);
  CAN_SnifferMessage receivedMessageBuffer[MAX_MESSAGE_BUFFER_SIZE];

  fr = f_mount(&SDFatFS, "0:", 1);
  if (fr == FR_OK)
  {
    HAL_GPIO_WritePin(GPIOG, GPIO_PIN_6, GPIO_PIN_RESET);
  }

  while(1)
  {
    if (receivedMessages >= MAX_MESSAGE_BUFFER_SIZE)
    {
      HAL_RTC_GetDate(&hrtc, &dateStamp, RTC_FORMAT_BIN);
      sprintf(filename, "%s20%02d%02d%02d.txt", SDPath, dateStamp.Year,
          dateStamp.Month, dateStamp.Date);
      fr = f_open(&SDFile, filename, FA_READ | FA_WRITE | FA_OPEN_APPEND);

      // Look for broken messages in log file
      if ((f_size(&SDFile) % sizeof(CAN_SnifferMessage)) == 0)
      {
        // Remove broken messages in log file
        const FSIZE_t offset = f_size(&SDFile)/sizeof(CAN_SnifferMessage) * sizeof(CAN_SnifferMessage);
        f_lseek(&SDFile, offset);
        f_truncate(&SDFile);
      }

      fr |= f_write(&SDFile, receivedMessageBuffer, bufferSize, &bw);
      fr |= f_close(&SDFile);

      if ((fr == FR_OK) && (bw == bufferSize))
      {
        HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_4);
      }

      receivedMessages = 0;
    }

    osMessageQueueGet(msgQueueStorageId, &receivedMessageBuffer[receivedMessages], 0U, osWaitForever);
    receivedMessages++;
  }

  fr = f_unmount(SDPath);
}
//--------------------------------------------------------------------------------------------------
