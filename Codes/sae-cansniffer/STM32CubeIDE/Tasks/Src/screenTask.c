#include "screenTask.h"
#include "main.h"
#include "stm32f4xx_hal_rtc.h"
#include "stm32_CAN_SnifferMessage.h"
#include "FreeRTOS.h"
#include "event_groups.h"
#include "eventFlags.h"
#include "screenStates.h"
//--------------------------------------------------------------------------------------------------
extern osMessageQueueId_t msgQueueHighCANId;
//--------------------------------------------------------------------------------------------------
extern osMessageQueueId_t speedMsgQueue;
extern osMessageQueueId_t gearMsgQueue;
extern osMessageQueueId_t oilTempMsgQueue;
extern osMessageQueueId_t airTempMsgQueue;
extern osMessageQueueId_t waterTempMsgQueue;
extern osMessageQueueId_t fuelLevelMsgQueue;
extern osMessageQueueId_t rpmMsgQueue;
//--------------------------------------------------------------------------------------------------
extern EventGroupHandle_t currentMessage;
extern EventGroupHandle_t nextMessage;
//--------------------------------------------------------------------------------------------------
#define WATER_TEMP_MSG_ID 0x206
#define OIL_TEMP_MSG_ID 0x203
#define AIR_TEMP_MSG_ID 0x201
#define SPEED_MSG_ID 0x301
#define RPM_MSG_ID 0x2
#define GEAR_MSG_ID 0x202
#define FUEL_LEVEL_MSG_ID 0
//--------------------------------------------------------------------------------------------------
/* Definitions for screen */
const osThreadAttr_t screen_attributes = {
  .name = "screenTask",
  .priority = (osPriority_t) osPriorityAboveNormal,
  .stack_size = 128 * 4
};
//--------------------------------------------------------------------------------------------------
/**
* @brief Task responsible for updating screen
* @param argument: Not used
* @retval None
*/
void screenTask(void *argument)
{
  CAN_SnifferMessage receivedMessage;

  unsigned int speed = 0;
  unsigned int gear = 0;
  unsigned int waterTemp = 0;
  unsigned int oilTemp = 0;
  unsigned int airTemp = 0;
  unsigned int fuelLevel = 0;
  unsigned int rpm = 0;

  while(1)
  {
    osMessageQueuePeek(msgQueueHighCANId, &receivedMessage, 0U, osWaitForever);

    // osMessageQueuePut(errorLedMsgQueue, &ledState, 0U, 0);

    switch(receivedMessage.canID)
    {
      case WATER_TEMP_MSG_ID:
        waterTemp = receivedMessage.CAN_Payload[0];
        osMessageQueuePut(waterTempMsgQueue, &waterTemp, 0U, 0);
        break;

      case OIL_TEMP_MSG_ID:
        oilTemp = receivedMessage.CAN_Payload[0];
        osMessageQueuePut(oilTempMsgQueue, &oilTemp, 0U, 0);
        break;

      case AIR_TEMP_MSG_ID:
        airTemp = receivedMessage.CAN_Payload[0];
        osMessageQueuePut(airTempMsgQueue, &airTemp, 0U, 0);
        break;

      case SPEED_MSG_ID:
        speed = receivedMessage.CAN_Payload[0];
        osMessageQueuePut(speedMsgQueue, &speed, 0U, 0);
        break;

      case RPM_MSG_ID:
        rpm = 51*((receivedMessage.CAN_Payload[1] << 8) | receivedMessage.CAN_Payload[0]);
        osMessageQueuePut(rpmMsgQueue, &rpm, 0U, 0);
        break;

      case GEAR_MSG_ID:
        gear = receivedMessage.CAN_Payload[0];
        osMessageQueuePut(gearMsgQueue, &gear, 0U, 0);
        break;

      case FUEL_LEVEL_MSG_ID:
        // //todo: update fuelLevel value
        // fuelLevel = 0;
        // osMessageQueuePut(fuelLevelMsgQueue, &fuelLevel, 0U, 0);
        break;

      default:
        break;
    }

    xEventGroupSync(currentMessage, SCREEN_TASK_BIT, HIGH_MSG_SYNC_BITS, osWaitForever);
    xEventGroupSync(nextMessage, SCREEN_TASK_BIT, HIGH_MSG_SYNC_BITS, osWaitForever);
  }
}
//--------------------------------------------------------------------------------------------------
