//--------------------------------------------------------------------------------------------------
/**
  * File Name          : tasks_start.c
  * Description        : Code for freertos applications
**/
//--------------------------------------------------------------------------------------------------
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"
#include "event_groups.h"

#include "debugTask.h"
#include "msgBuilderTask.h"
#include "uartDebugTask.h"
#include "uartRawTxTask.h"
#include "messageRemoverTask.h"
#include "storageTask.h"
#include "screenTask.h"
//--------------------------------------------------------------------------------------------------
osMessageQueueId_t msgQueueLowCANId;
osMessageQueueId_t msgQueueHighCANId;
osMessageQueueId_t msgQueueDebugId;
osMessageQueueId_t msgQueueStorageId;

EventGroupHandle_t currentMessage;
EventGroupHandle_t nextMessage;
osEventFlagsId_t uartEventFlagId;
osEventFlagsId_t debugCanActiveEventFlagId;
//--------------------------------------------------------------------------------------------------
/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* Create the queue(s) */
  // TOTAL SIZE: 32 * 16 = 512 bytes
  msgQueueLowCANId = osMessageQueueNew(16, sizeof(CAN_SnifferMessage), NULL);
  // TOTAL SIZE: 32 * 16 = 512 bytes
  msgQueueHighCANId = osMessageQueueNew(16, sizeof(CAN_SnifferMessage), NULL);
  // TOTAL SIZE: 32 * 16 = 512 bytes
  msgQueueDebugId = osMessageQueueNew(16, sizeof(CAN_SnifferMessage), NULL);
  // TOTAL SIZE: 32 * 50 = 1600 bytes
  msgQueueStorageId = osMessageQueueNew(50, sizeof(CAN_SnifferMessage), NULL);

  /* Create the event group(s) */
  currentMessage = (EventGroupHandle_t)osEventFlagsNew(NULL);
  nextMessage = (EventGroupHandle_t)osEventFlagsNew(NULL);
  uartEventFlagId = osEventFlagsNew(NULL);
  debugCanActiveEventFlagId = osEventFlagsNew(NULL);

  /* Create the thread(s) */
  debugTaskHandle = osThreadNew(debugTask, NULL, &debugTask_attributes);
  msgBuilderHandle = osThreadNew(msgBuilderTask, NULL, &msgBuilder_attributes);
  uartRawTxHandle = osThreadNew(uartRawTxTask, NULL, &uartRawTx_attributes);
  uartDebugHandle = osThreadNew(uartDebugTask, NULL, &uartDebug_attributes);
  storageTaskHandle = osThreadNew(storageTask, NULL, &storageTask_attributes);
  messageRemoverTaskHandle = osThreadNew(messageRemoverTask, NULL, &messageRemoverTask_attributes);
  screenTaskHandle = osThreadNew(screenTask, NULL, &screenTask_attributes);
}
//--------------------------------------------------------------------------------------------------
