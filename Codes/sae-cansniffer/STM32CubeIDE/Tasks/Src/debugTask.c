#include "debugTask.h"
#include "main.h"
#include "stm32f4xx_hal_rtc.h"
#include "stm32_CAN_SnifferMessage.h"

extern RTC_HandleTypeDef hrtc;
extern osMessageQueueId_t msgQueueLowCANId;
extern osMessageQueueId_t msgQueueHighCANId;
extern osMessageQueueId_t msgQueueDebugId;

uint32_t msgCountLow;
uint32_t msgCountHigh;
uint32_t msgCountDebug;

/*FIXME: REMOVE THIS DEBUG TASK*/
/* Definitions for debugTiming */
//osThreadId_t debugTaskHandle;
const osThreadAttr_t debugTask_attributes = {
  .name = "debugTask",
  .priority = (osPriority_t) osPriorityHigh,
  .stack_size = 128 * 4
};

/*FIXME: Remove Debug Task*/
/**
* @brief
* @param argument: Not used
* @retval None
*/
void debugTask(void *argument)
{
	RTC_DateTypeDef DateStamp;
	RTC_TimeTypeDef TimeStamp;

	while(1)
	{
		HAL_RTC_GetTime(&hrtc, &TimeStamp, RTC_FORMAT_BIN);
		HAL_RTC_GetDate(&hrtc, &DateStamp,RTC_FORMAT_BIN);

		osDelay(50);

		msgCountLow = osMessageQueueGetCount(msgQueueLowCANId);
		msgCountHigh = osMessageQueueGetCount(msgQueueHighCANId);
		msgCountDebug = osMessageQueueGetCount(msgQueueDebugId);
	}
}
