#include "uartDebugTask.h"
#include "main.h"
#include "eventFlags.h"
#include "string.h"
#include <stdio.h>
#include <stdlib.h>

extern RTC_HandleTypeDef hrtc;
extern UART_HandleTypeDef huart3;
extern osMessageQueueId_t msgQueueDebugId;
extern osEventFlagsId_t debugCanActiveEventFlagId;

/* Definitions for uartDebug */
/*FIXME: Test thread priority mode (is this setting good?)*/
/*FIXME: Is this stack size good ?*/
const osThreadAttr_t uartDebug_attributes = {
  .name = "uartDebug",
  .priority = (osPriority_t) osPriorityAboveNormal,
  .stack_size = 256 * 4
};

const osEventFlagsAttr_t uartEventFlag_attributes = {
		.name = "uartEventFlag"
};

void uartDebugTask(void *argument)
{
	CAN_SnifferMessage msgToPrint;
	const char rxCmdString[] = "RX CMD: ";
	char outputBuffer[100];
	char inputBuffer[32];
	uint32_t errorFlag;

	/*Input Buffer Init*/
	clearBuffer(inputBuffer,sizeof(inputBuffer));
	/* Set inputBuffer to receive incoming messages in non-blocking mode */
	HAL_UART_Receive_IT(&huart3,(uint8_t *) &inputBuffer, sizeof(inputBuffer));

	/* Put task in sleep mode until a message is received */
	osEventFlagsWait(uartEventFlagId, 1, osFlagsWaitAny, osWaitForever);
	/* Transmit welcome string & ask for command input */
	HAL_UART_Transmit(&huart3, (uint8_t *) "WELCOME TO DEBUG MODE - CMD INPUT\n", 34, osWaitForever);

	while(1)
	{
		/* Reset current transmission */
		HAL_UART_AbortReceive_IT(&huart3);
		/* Clear Buffer*/
		clearBuffer(inputBuffer,sizeof(inputBuffer));

		/* Set inputBuffer to receive incoming messages in non-blocking mode */
		HAL_UART_Receive_IT(&huart3,(uint8_t *) &inputBuffer, sizeof(inputBuffer));

		/* Put task in sleep mode until a message is received */
		osEventFlagsWait(uartEventFlagId, 1, osFlagsWaitAny, osWaitForever);

		/* Echo Received Message */
		strcpy(outputBuffer, rxCmdString);
		strcat(outputBuffer, inputBuffer);
		HAL_UART_Transmit(&huart3, (uint8_t *) outputBuffer, strlen(outputBuffer), osWaitForever);

		/* Command Interpretation */
		if (strcmp(inputBuffer, "SET RTC\n") == 0)
		{
			setRTC(inputBuffer,sizeof(inputBuffer));
			HAL_UART_Transmit(&huart3, (uint8_t *) "\nRTC SET\n", 9, osWaitForever);
		}

		else if(strcmp(inputBuffer, "PRINT CAN\n") == 0)
		{
			HAL_UART_Transmit(&huart3, (uint8_t *) "SEND STOP TO CANCEL\n", 20, osWaitForever);

			osEventFlagsSet(debugCanActiveEventFlagId, UART_DEBUG_TASK_BIT);
			while(1)
			{
				HAL_UART_AbortReceive_IT(&huart3);
				/* Clear Buffer*/
				clearBuffer(inputBuffer,sizeof(inputBuffer));

				/* Set inputBuffer to receive incoming messages in non-blocking mode */
				HAL_UART_Receive_IT(&huart3,(uint8_t *) &inputBuffer, sizeof(inputBuffer));
				errorFlag = osEventFlagsWait(uartEventFlagId, 1, osFlagsWaitAny, 0U);

				if (errorFlag != 1) // 1 = waited event from UART CMD INPUT
				{
					/* TODO: TRY TO CONSUME DATA FROM QUEUE */
					errorFlag = osMessageQueueGet(msgQueueDebugId, &msgToPrint, 0U, 0U);

					if(errorFlag == osOK)
					{
						/* PRINT MESSAGE */
						messageToString(outputBuffer, msgToPrint);
						HAL_UART_Transmit(&huart3, (uint8_t*) outputBuffer, (uint16_t) strlen(outputBuffer), osWaitForever);
					}
				}
				else if (strcmp(inputBuffer, "STOP\n")==0)
				{
					/* STOP CMD RECEIVED */
					HAL_UART_Transmit(&huart3, (uint8_t *) "STOPPING PRINT CAN\n", 19, osWaitForever);
					osEventFlagsClear(debugCanActiveEventFlagId, UART_DEBUG_TASK_BIT);
					break;
				}
			}
		}
		else
		{
			HAL_UART_Transmit(&huart3, (uint8_t *) "CMD NOT VALID\n", 14, osWaitForever);
		}
	}
}

void intToString(uint8_t *destString, uint32_t number, uint32_t base)
{
	int i,j;
	int modulo;
	uint8_t tempString[11];

	for(i = 0; number > 0; i++)
	{
		modulo = number % base;
		if(modulo < 10)
		{
			tempString[i] = modulo + 48;
		}
		else
		{
			tempString[i] = modulo + 55;
		}
		number = number/base;
	}

	i=i-1;

	for(j=0;i>-1;i--)
	{
		*(uint8_t*)(destString + j) = tempString[i];
		j++;
	}

	*(uint8_t*)(destString + j) = '\0';
}

void clearBuffer(char *buffer, uint8_t bufferSize)
{
	int i;
	for(i = 0; i< bufferSize; i++)
	{
		buffer[i] = 0;	// ASCII null character
	}
}

void messageToString(char *outputBuffer, CAN_SnifferMessage msgToPrint)
{
	int i;
	char *stringMsgHeaders;
	char auxBuffer[24];

	stringMsgHeaders = "\nStdid : ";
	strcpy(outputBuffer,stringMsgHeaders);
	intToString((uint8_t*)auxBuffer,msgToPrint.canID, 16);
	strcat(outputBuffer,auxBuffer);

	stringMsgHeaders = "\nDLC : ";
	strcat(outputBuffer, stringMsgHeaders);
	intToString((uint8_t*)auxBuffer,msgToPrint.DLC, 10);
	strcat(outputBuffer, auxBuffer);

	stringMsgHeaders = "\nPayload : ";
	strcat(outputBuffer, stringMsgHeaders);
	for(i = 0; i < msgToPrint.DLC; i++)
	{
		stringMsgHeaders = "[";
		strcat(outputBuffer, stringMsgHeaders);
		intToString((uint8_t*)auxBuffer,msgToPrint.CAN_Payload[i],16);
		strcat(outputBuffer, auxBuffer);
		stringMsgHeaders = "]";
		strcat(outputBuffer, stringMsgHeaders);
	}
}

void setRTC(char *inputBuffer, uint8_t bufferSize)
{
	RTC_DateTypeDef rtcDate;
	RTC_TimeTypeDef rtcTime;

	/*TODO: Add input protection for values out of bounds*/
	/* Reset current transmission */
	HAL_UART_AbortReceive_IT(&huart3);
	/* PROMPT YEAR */
	HAL_UART_Transmit(&huart3, (uint8_t *) "\nYEAR [00-99]:", 14, osWaitForever);
	/* Clear Buffer*/
	clearBuffer(inputBuffer,bufferSize);
	/* Set inputBuffer to receive incoming messages in non-blocking mode */
	HAL_UART_Receive_IT(&huart3,(uint8_t *) inputBuffer, bufferSize);
	/* Put task in sleep mode until a message is received */
	osEventFlagsWait(uartEventFlagId, 1, osFlagsWaitAny, osWaitForever);
	/* ASSERT RX DATA */
	rtcDate.Year = atoi(inputBuffer);

	/* PROMPT MONTH */
	HAL_UART_Transmit(&huart3, (uint8_t *) "\nMONTH [1 - 12]:", 16, osWaitForever);
	/* Reset current transmission */
	HAL_UART_AbortReceive_IT(&huart3);
	/* Clear Buffer*/
	clearBuffer(inputBuffer,bufferSize);
	/* Set inputBuffer to receive incoming messages in non-blocking mode */
	HAL_UART_Receive_IT(&huart3,(uint8_t *) inputBuffer, bufferSize);
	/* Put task in sleep mode until a message is received */
	osEventFlagsWait(uartEventFlagId, 1, osFlagsWaitAny, osWaitForever);
	/* ASSERT RX DATA */
	rtcDate.Month = atoi(inputBuffer);

	/* PROMPT DAY */
	HAL_UART_Transmit(&huart3, (uint8_t *) "\nDAY [1 - 31]:", 13, osWaitForever);
	/* Reset current transmission */
	HAL_UART_AbortReceive_IT(&huart3);
	/* Clear Buffer*/
	clearBuffer(inputBuffer,bufferSize);
	/* Set inputBuffer to receive incoming messages in non-blocking mode */
	HAL_UART_Receive_IT(&huart3,(uint8_t *) inputBuffer, bufferSize);
	/* Put task in sleep mode until a message is received */
	osEventFlagsWait(uartEventFlagId, 1, osFlagsWaitAny, osWaitForever);
	/* ASSERT RX DATA */
	rtcDate.Date = atoi(inputBuffer);

	/* PROMPT HOUR */
	HAL_UART_Transmit(&huart3, (uint8_t *) "\nHOUR [0 - 23]:", 15, osWaitForever);
	/* Reset current transmission */
	HAL_UART_AbortReceive_IT(&huart3);
	/* Clear Buffer*/
	clearBuffer(inputBuffer,bufferSize);
	/* Set inputBuffer to receive incoming messages in non-blocking mode */
	HAL_UART_Receive_IT(&huart3,(uint8_t *) inputBuffer, bufferSize);
	/* Put task in sleep mode until a message is received */
	osEventFlagsWait(uartEventFlagId, 1, osFlagsWaitAny, osWaitForever);
	/* ASSERT RX DATA */
	rtcTime.Hours = atoi(inputBuffer);

	/* PROMPT MINUTE */
	HAL_UART_Transmit(&huart3, (uint8_t *) "\nMINUTES [0 - 59]:", 18, osWaitForever);
	/* Reset current transmission */
	HAL_UART_AbortReceive_IT(&huart3);
	/* Clear Buffer*/
	clearBuffer(inputBuffer,bufferSize);
	/* Set inputBuffer to receive incoming messages in non-blocking mode */
	HAL_UART_Receive_IT(&huart3,(uint8_t *) inputBuffer, bufferSize);
	/* Put task in sleep mode until a message is received */
	osEventFlagsWait(uartEventFlagId, 1, osFlagsWaitAny, osWaitForever);
	/* ASSERT RX DATA */
	rtcTime.Minutes = atoi(inputBuffer);

	/* PROMPT SECONDS */
	HAL_UART_Transmit(&huart3, (uint8_t *) "\nSECONDS [0 - 59]:", 18, osWaitForever);
	/* Reset current transmission */
	HAL_UART_AbortReceive_IT(&huart3);
	/* Clear Buffer*/
	clearBuffer(inputBuffer,bufferSize);
	/* Set inputBuffer to receive incoming messages in non-blocking mode */
	HAL_UART_Receive_IT(&huart3,(uint8_t *) inputBuffer, bufferSize);
	/* Put task in sleep mode until a message is received */
	osEventFlagsWait(uartEventFlagId, 1, osFlagsWaitAny, osWaitForever);
	/* ASSERT RX DATA */
	rtcTime.Seconds = atoi(inputBuffer);

	/*SET RTC HARDWARE*/
	HAL_RTC_SetDate(&hrtc, &rtcDate, RTC_FORMAT_BIN);
	HAL_RTC_SetTime(&hrtc, &rtcTime, RTC_FORMAT_BIN);
}
