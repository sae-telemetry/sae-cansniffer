#include "msgBuilderTask.h"
#include "main.h"
#include "stm32_CAN_SnifferMessage.h"

extern RTC_HandleTypeDef hrtc;
extern CRC_HandleTypeDef hcrc;
extern osMessageQueueId_t msgQueueLowCANId;
extern osMessageQueueId_t msgQueueHighCANId;

/*TODO: SWD DEBUG - REMOVE*/
CAN_SnifferMessage rxCANMessage;

/* Definitions for messageBuilder */
osThreadId_t msgBuilderHandle;
const osThreadAttr_t msgBuilder_attributes = {
  .name = "MessageBuilder",
  .priority = (osPriority_t) osPriorityHigh,
  .stack_size = 128 * 4
};

/**
* @brief
* @param argument: Not used
* @retval None
*/
void msgBuilderTask(void *argument)
{
    //CAN_SnifferMessage rxCANMessage;
    RTC_TimeTypeDef rtcTime;
    RTC_DateTypeDef rtcDate;

    while(1)
    {
        osMessageQueueGet(msgQueueLowCANId, &rxCANMessage, 0U, osWaitForever);

        /* GET TIMESTAMP DATA */
        HAL_RTC_GetTime(&hrtc, &rtcTime, RTC_FORMAT_BIN);
        HAL_RTC_GetDate(&hrtc, &rtcDate, RTC_FORMAT_BIN);

        /* FILL DATA STRUCTURE */
        rxCANMessage.Date = rtcDate.Date;
        rxCANMessage.Month = rtcDate.Month;
        rxCANMessage.Year = rtcDate.Year;
        rxCANMessage.Hours = rtcTime.Hours;
        rxCANMessage.Minutes = rtcTime.Minutes;
        rxCANMessage.Seconds = rtcTime.Seconds;
        rxCANMessage.SubSeconds = (uint8_t) rtcTime.SubSeconds;  // max value is always less than 255

        /* CALCULATE MESSAGE CRC */
        rxCANMessage.CRC_Value = HAL_CRC_Calculate(&hcrc, &rxCANMessage, sizeof(CAN_SnifferMessage)/sizeof(uint32_t) - 1); // Discard CRC field of data structure

        /*OUTPUT HIGH LEVEL MESSAGE*/
        osMessageQueuePut(msgQueueHighCANId, &rxCANMessage, 0U, osWaitForever);
    }
}
