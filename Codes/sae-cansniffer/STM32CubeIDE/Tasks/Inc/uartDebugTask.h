#ifndef __uartDebugTask_H
#define __uartDebugTask_H
#ifdef __cplusplus
 extern "C" {
#endif

#include "cmsis_os2.h"
#include "stm32_CAN_SnifferMessage.h"

osThreadId_t uartDebugHandle;
osEventFlagsId_t uartEventFlagId;
const osThreadAttr_t uartDebug_attributes;
const osEventFlagsAttr_t uartEventFlag_attributes;

void uartDebugTask(void *argument);
void intToString(uint8_t *destString, uint32_t number, uint32_t base);
void clearBuffer(char  *buffer, uint8_t bufferSize);
void messageToString(char *outputBuffer, CAN_SnifferMessage msgToPrint);
void setRTC(char *inputBuffer, uint8_t bufferSize);

#ifdef __cplusplus
}
#endif
#endif /*__uartDebugTask_H */
