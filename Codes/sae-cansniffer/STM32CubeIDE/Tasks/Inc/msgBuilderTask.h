#ifndef __msgBuilderTask_H
#define __msgBuilderTask_H
#ifdef __cplusplus
 extern "C" {
#endif

#include "cmsis_os2.h"

osThreadId_t msgBuilderHandle;
const osThreadAttr_t msgBuilder_attributes;

void msgBuilderTask(void *argument);

#ifdef __cplusplus
}
#endif
#endif /*__msgBuilderTask_H */
