#ifndef __MESSAGE_REMOVER_TASK_H__
#define __MESSAGE_REMOVER_TASK_H__
#ifdef __cplusplus
  extern "C" {
#endif

#include "cmsis_os2.h"

osThreadId_t messageRemoverTaskHandle;
const osThreadAttr_t messageRemoverTask_attributes;

void messageRemoverTask(void *argument);

#ifdef __cplusplus
}
#endif

#endif /*__MESSAGE_REMOVER_TASK_H__ */
