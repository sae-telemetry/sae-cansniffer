#ifndef __SCREEN_TASK__H__
#define __SCREEN_TASK__H__
#ifdef __cplusplus
  extern "C" {
#endif

#include "cmsis_os2.h"

osThreadId_t screenTaskHandle;
const osThreadAttr_t screenTask_attributes;

void screenTask(void *argument);

#ifdef __cplusplus
}
#endif

#endif //__SCREEN_TASK__H__
