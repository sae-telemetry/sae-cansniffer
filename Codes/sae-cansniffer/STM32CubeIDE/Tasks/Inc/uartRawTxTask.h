#ifndef __uartRawTxTask_H
#define __uartRawTxTask_H
#ifdef __cplusplus
 extern "C" {
#endif

#include "cmsis_os2.h"

osThreadId_t uartRawTxHandle;
const osThreadAttr_t uartRawTx_attributes;

void uartRawTxTask(void *argument);

#ifdef __cplusplus
}
#endif
#endif /*__uartRawTxTask_H */
