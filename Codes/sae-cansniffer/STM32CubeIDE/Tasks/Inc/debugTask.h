#ifndef __debug_taks_H
#define __debug_taks_H
#ifdef __cplusplus
 extern "C" {
#endif

#include "cmsis_os2.h"

osThreadId_t debugTaskHandle;
const osThreadAttr_t debugTask_attributes;

void debugTask(void *argument);

#ifdef __cplusplus
}
#endif
#endif /*__ can_H */
