#ifndef __STORAGE_TASK__H__
#define __STORAGE_TASK__H__
#ifdef __cplusplus
  extern "C" {
#endif

#include "cmsis_os2.h"

osThreadId_t storageTaskHandle;
const osThreadAttr_t storageTask_attributes;

void storageTask(void *argument);

#ifdef __cplusplus
}
#endif

#endif //__STORAGE_TASK__H__
