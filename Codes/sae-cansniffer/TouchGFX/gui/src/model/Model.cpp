#include <gui/model/Model.hpp>
#include <gui/model/ModelListener.hpp>

#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"
#include "screenStates.h"

unsigned int speed = 0;
unsigned int gear = 0;
unsigned int waterTemp = 0;
unsigned int oilTemp = 0;
unsigned int airTemp = 0;
unsigned int fuelLevel = 0;
unsigned int rpm = 0;
Error_LedTypedef ledState;

extern "C"
{
    osMessageQueueId_t speedMsgQueue;
    osMessageQueueId_t gearMsgQueue;
    osMessageQueueId_t errorLedMsgQueue;
    osMessageQueueId_t oilTempMsgQueue;
    osMessageQueueId_t waterTempMsgQueue;
    osMessageQueueId_t fuelLevelMsgQueue;
    osMessageQueueId_t rpmMsgQueue;
    osMessageQueueId_t airTempMsgQueue;
}

Model::Model() : modelListener(0)
{
	speedMsgQueue = osMessageQueueNew(1, sizeof(unsigned int), NULL);
    gearMsgQueue = osMessageQueueNew(1, sizeof(unsigned int), NULL);
    errorLedMsgQueue = osMessageQueueNew(1, sizeof(Error_LedTypedef), NULL);
    oilTempMsgQueue = osMessageQueueNew(1, sizeof(unsigned int), NULL);
    airTempMsgQueue = osMessageQueueNew(1, sizeof(unsigned int), NULL);
    waterTempMsgQueue = osMessageQueueNew(1, sizeof(unsigned int), NULL);
    rpmMsgQueue = osMessageQueueNew(1, sizeof(unsigned int), NULL);
    fuelLevelMsgQueue = osMessageQueueNew(1, sizeof(unsigned int), NULL);
}

void Model::tick()
{
    if(osMessageQueueGet(speedMsgQueue, &speed, 0U, 0) == osOK)
    {
        modelListener->updateSpeed(speed);
    }
    else
    {
        // no message available
    }

    if(osMessageQueueGet(gearMsgQueue, &gear, 0U, 0) == osOK)
    {
        modelListener->updateGear(gear);
    }
    else
    {
        // no message available
    }

    if(osMessageQueueGet(errorLedMsgQueue, &ledState, 0U, 0) == osOK)
    {
        modelListener->enableErrorLed(ledState.ledId, static_cast<bool>(ledState.enabled));
    }
    else
    {
        // no message available
    }

    if(osMessageQueueGet(oilTempMsgQueue, &oilTemp, 0U, 0) == osOK)
    {
        modelListener->updateOilTemp(oilTemp);
    }
    else
    {
        // no message available
    }

    if(osMessageQueueGet(airTempMsgQueue, &airTemp, 0U, 0) == osOK)
    {
        modelListener->updateAirTemp(airTemp);
    }
    else
    {
        // no message available
    }

    if(osMessageQueueGet(waterTempMsgQueue, &waterTemp, 0U, 0) == osOK)
    {
        modelListener->updateWaterTemp(waterTemp);
    }
    else
    {
        // no message available
    }

    if(osMessageQueueGet(fuelLevelMsgQueue, &fuelLevel, 0U, 0) == osOK)
    {
        modelListener->updateFuelLevel(fuelLevel);
    }
    else
    {
        // no message available
    }

    if(osMessageQueueGet(rpmMsgQueue, &rpm, 0U, 0) == osOK)
    {
        modelListener->updateRpm(rpm);
    }
    else
    {
        // no message available
    }
}
