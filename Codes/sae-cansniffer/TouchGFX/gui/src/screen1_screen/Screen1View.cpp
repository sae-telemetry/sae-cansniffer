#include <gui/screen1_screen/Screen1View.hpp>
#include <touchgfx/Color.hpp>

Screen1View::Screen1View()
{

}

void Screen1View::setupScreen()
{
    Screen1ViewBase::setupScreen();
}

void Screen1View::tearDownScreen()
{
    Screen1ViewBase::tearDownScreen();
}

void Screen1View::updateSpeed(unsigned int speed)
{
    Unicode::snprintf(speed_labelBuffer, SPEED_LABEL_SIZE, "%d", speed);
    // speed_label.resizeToCurrentText();
    speed_label.invalidate();
}

void Screen1View::updateGear(unsigned int gear)
{
    static unsigned int lastGear = 0;

    if (gear != lastGear)
    {
        enableGear(gear, true);
        enableGear(lastGear, false);
        lastGear = gear;
    }
}

void Screen1View::enableGear(unsigned int gear, bool enable)
{
    bool valid = true;
    touchgfx::TextArea* gearText;
    switch(gear)
    {
        case 1:
            gearText = &gear_1;
            break;

        case 2:
            gearText = &gear_2;
            break;

        case 3:
            gearText = &gear_3;
            break;

        case 4:
            gearText = &gear_4;
            break;

        case 5:
            gearText = &gear_5;
            break;

        default:
            valid = false;
            break;
    }

    if (valid)
    {
        if (enable)
        {
            gearText->setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
        }
        else
        {
            gearText->setColor(touchgfx::Color::getColorFrom24BitRGB(64, 64, 64));
        }
        gearText->invalidate();
    }
}

void Screen1View::enableErrorLed(unsigned int led, bool enable)
{
    bool valid = true;
    touchgfx::Circle* ledComponent;
    touchgfx::PainterRGB565* ledComponentPainter;

    switch(led)
    {
        case 1:
            ledComponentPainter = &led_1Painter;
            ledComponent = &led_1;
            break;

        case 2:
            ledComponentPainter = &led_2Painter;
            ledComponent = &led_2;
            break;

        case 3:
            ledComponentPainter = &led_3Painter;
            ledComponent = &led_3;
            break;

        case 4:
            ledComponentPainter = &led_4Painter;
            ledComponent = &led_4;
            break;

        case 5:
            ledComponentPainter = &led_5Painter;
            ledComponent = &led_5;
            break;

        case 6:
            ledComponentPainter = &led_6Painter;
            ledComponent = &led_6;
            break;

        case 7:
            ledComponentPainter = &led_7Painter;
            ledComponent = &led_7;
            break;

        case 8:
            ledComponentPainter = &led_8Painter;
            ledComponent = &led_8;
            break;

        default:
            valid = false;
            break;
    }

    if (valid)
    {
        if (enable)
        {
            ledComponentPainter->setColor(touchgfx::Color::getColorFrom24BitRGB(255, 0, 0));
        }
        else
        {
            ledComponentPainter->setColor(touchgfx::Color::getColorFrom24BitRGB(64, 64, 64));
        }
        ledComponent->invalidate();
    }
}

void Screen1View::updateOilTemp(unsigned int temp)
{
    oil_temp_bar.setValue(temp);
    oil_temp_bar.invalidate();
}

void Screen1View::updateWaterTemp(unsigned int temp)
{
    water_temp_bar.setValue(temp);
    water_temp_bar.invalidate();
}

void Screen1View::updateFuelLevel(unsigned int level)
{
    fuel_lvl_bar.setValue(level);
    fuel_lvl_bar.invalidate();
}

void Screen1View::updateRpm(unsigned int rpm)
{
    rpm_bar.setValue(rpm);
    rpm_bar.invalidate();
}

void Screen1View::updateAirTemp(unsigned int temp)
{
    air_temp.setValue(temp);
    air_temp.invalidate();
}
