#include <gui/screen1_screen/Screen1View.hpp>
#include <gui/screen1_screen/Screen1Presenter.hpp>

Screen1Presenter::Screen1Presenter(Screen1View& v)
    : view(v)
{

}

void Screen1Presenter::activate()
{

}

void Screen1Presenter::deactivate()
{

}

void Screen1Presenter::updateSpeed(unsigned int speed)
{
    view.updateSpeed(speed);
}

void Screen1Presenter::updateGear(unsigned int gear)
{
    view.updateGear(gear);
}

void Screen1Presenter::enableErrorLed(unsigned int led, bool enable)
{
    view.enableErrorLed(led, enable);
}

void Screen1Presenter::updateOilTemp(unsigned int temp)
{
    view.updateOilTemp(temp);
}

void Screen1Presenter::updateWaterTemp(unsigned int temp)
{
    view.updateWaterTemp(temp);
}

void Screen1Presenter::updateFuelLevel(unsigned int level)
{
    view.updateFuelLevel(level);
}

void Screen1Presenter::updateRpm(unsigned int rpm)
{
    view.updateRpm(rpm);
}

void Screen1Presenter::updateAirTemp(unsigned int temp)
{
    view.updateAirTemp(temp);
}