#ifndef SCREEN1VIEW_HPP
#define SCREEN1VIEW_HPP

#include <gui_generated/screen1_screen/Screen1ViewBase.hpp>
#include <gui/screen1_screen/Screen1Presenter.hpp>

class Screen1View : public Screen1ViewBase
{
public:
    Screen1View();
    virtual ~Screen1View() {}
    virtual void setupScreen();
    virtual void tearDownScreen();

    void updateSpeed(unsigned int speed);
    void updateGear(unsigned int gear);
    void enableErrorLed(unsigned int led, bool enable);
    void updateOilTemp(unsigned int temp);
    void updateWaterTemp(unsigned int temp);
    void updateFuelLevel(unsigned int level);
    void updateRpm(unsigned int rpm);
    void updateAirTemp(unsigned int temp);

protected:

private:
    void enableGear(unsigned int gear, bool enable);
};

#endif // SCREEN1VIEW_HPP
