#ifndef MODELLISTENER_HPP
#define MODELLISTENER_HPP

#include <gui/model/Model.hpp>

class ModelListener
{
public:
    ModelListener() : model(0) {}

    virtual ~ModelListener() {}

    void bind(Model* m)
    {
        model = m;
    }

    virtual void updateSpeed(unsigned int speed) {}
    virtual void updateGear(unsigned int gear) {}
    virtual void enableErrorLed(unsigned int led, bool enable) {}
    virtual void updateOilTemp(unsigned int temp) {}
    virtual void updateWaterTemp(unsigned int temp) {}
    virtual void updateFuelLevel(unsigned int level) {}
    virtual void updateRpm(unsigned int rpm) {}
    virtual void updateAirTemp(unsigned int temp) {}

protected:
    Model* model;
};

#endif // MODELLISTENER_HPP
