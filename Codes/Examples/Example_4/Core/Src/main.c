/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stm32_FIFO_static.h"
#include "string.h"
#include <stdio.h>
#include <stdlib.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
CAN_HandleTypeDef hcan1;

TIM_HandleTypeDef htim2;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
uint64_t TimeStamp;	// This is used in stm32f4xx_it.c
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_CAN1_Init(void);
static void MX_TIM2_Init(void);
static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */
static void CANFilterConfig(void);
static void concatString(uint8_t *destString, const uint8_t *sourceString);
static void intToString(uint8_t *destString, uint32_t number, uint32_t base);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	CAN_MessageTypeDef msgToSend;
	uint8_t *stringMsgHeaders;
	uint8_t bufferUART[24];
	uint8_t stringUART[100];
	int i;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_CAN1_Init();
  MX_TIM2_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */
  CANFilterConfig();
  HAL_CAN_Start(&hcan1);
  HAL_CAN_ActivateNotification(&hcan1, CAN_IT_RX_FIFO0_MSG_PENDING);

  /*FIXME: FIFO Test*/
  CAN_RxFIFO_Init();
  HAL_TIM_Base_Start_IT(&htim2);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  	while(CAN_RxFIFO_GetPtr != CAN_RxFIFO_PutPtr)
  	{
  		/*stringMsgHeaders = (uint8_t*) "Received a CAN Message\n";
  		HAL_UART_Transmit(&huart2, stringMsgHeaders, strlen((char*)stringMsgHeaders), 10);*/

  		if(CAN_RxFIFO_Get(&msgToSend))
  		{
  			bufferUART[0] = '\0';
  			stringUART[0] = '\0';

  			stringMsgHeaders = (uint8_t*) "\nStdid : ";
  			concatString((uint8_t*) stringUART, stringMsgHeaders);
  			intToString((uint8_t*)bufferUART,msgToSend.CAN_Header.StdId,16);
  			concatString((uint8_t*) stringUART, (uint8_t*)bufferUART);

  			stringMsgHeaders = (uint8_t*) "\nDLC : ";
  			concatString((uint8_t*) stringUART, stringMsgHeaders);
  			intToString((uint8_t*)bufferUART,msgToSend.CAN_Header.DLC,10);
  			concatString((uint8_t*) stringUART, (uint8_t*)bufferUART);

  			stringMsgHeaders = (uint8_t*) "\nPayload : ";
  			concatString((uint8_t*) stringUART, stringMsgHeaders);
  			for(i = 0; i < msgToSend.CAN_Header.DLC; i++)
  			{
  				stringMsgHeaders = (uint8_t*) "[";
  				concatString((uint8_t*) stringUART, stringMsgHeaders);
  				intToString((uint8_t*)bufferUART,msgToSend.CAN_Payload[i],16);
  				concatString((uint8_t*) stringUART, (uint8_t*)bufferUART);
  				stringMsgHeaders = (uint8_t*) "]";
  				concatString((uint8_t*) stringUART, stringMsgHeaders);
  			}

  			HAL_UART_Transmit(&huart2, (uint8_t*) stringUART, (uint16_t) strlen((char*)stringUART), 10);
  		}
  		else
  		{
  			stringMsgHeaders = (uint8_t*) "Error while getting an element from FIFO\n";
  			HAL_UART_Transmit(&huart2, stringMsgHeaders, strlen((char*)stringMsgHeaders), 10);
  		}
  	}
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 128;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV16;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV16;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief CAN1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_CAN1_Init(void)
{

  /* USER CODE BEGIN CAN1_Init 0 */

  /* USER CODE END CAN1_Init 0 */

  /* USER CODE BEGIN CAN1_Init 1 */

  /* USER CODE END CAN1_Init 1 */
  hcan1.Instance = CAN1;
  hcan1.Init.Prescaler = 2;
  hcan1.Init.Mode = CAN_MODE_NORMAL;
  hcan1.Init.SyncJumpWidth = CAN_SJW_1TQ;
  hcan1.Init.TimeSeg1 = CAN_BS1_11TQ;
  hcan1.Init.TimeSeg2 = CAN_BS2_4TQ;
  hcan1.Init.TimeTriggeredMode = DISABLE;
  hcan1.Init.AutoBusOff = DISABLE;
  hcan1.Init.AutoWakeUp = DISABLE;
  hcan1.Init.AutoRetransmission = DISABLE;
  hcan1.Init.ReceiveFifoLocked = DISABLE;
  hcan1.Init.TransmitFifoPriority = DISABLE;
  if (HAL_CAN_Init(&hcan1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CAN1_Init 2 */

  /* USER CODE END CAN1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 79;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, LED_GREEN_Pin|LED_ORANGE_Pin|LED_RED_Pin|LED_BLUE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : LED_GREEN_Pin LED_ORANGE_Pin LED_RED_Pin LED_BLUE_Pin */
  GPIO_InitStruct.Pin = LED_GREEN_Pin|LED_ORANGE_Pin|LED_RED_Pin|LED_BLUE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
static void concatString(uint8_t *destString, const uint8_t *sourceString)
{
	/*FIXME: There must be a way to protect against memory overflow (in this case both sizes must sum up to a max of 100 elements)*/
	int size1 = 0;
	int size2 = 0;

	while(destString[size1] != '\0')
	{
		size1++;
	}

	while(sourceString[size2] != '\0')
	{
		destString[size1] = sourceString[size2];
		size1++;
		size2++;
	}
	destString[size1] = '\0';
}

static void intToString(uint8_t *destString, uint32_t number, uint32_t base)
{
	int i,j;
	int modulo;
	uint8_t tempString[11];

	for(i = 0; number > 0; i++)
	{
		modulo = number % base;
		if(modulo < 10)
		{
			tempString[i] = modulo + 48;
		}
		else
		{
			tempString[i] = modulo + 55;
		}
		number = number/base;
	}

	i=i-1;

	for(j=0;i>-1;i--)
	{
		*(uint8_t*)(destString + j) = tempString[i];
		j++;
	}

	*(uint8_t*)(destString + j) = '\0';
}

static void CANFilterConfig()
{
	CAN_FilterTypeDef myFilterConfig;
	myFilterConfig.FilterActivation = ENABLE;
	myFilterConfig.FilterBank = 14;
	myFilterConfig.FilterFIFOAssignment = 0;
	myFilterConfig.FilterIdHigh = 0x0000;
	myFilterConfig.FilterIdLow = 0x0000;
	myFilterConfig.FilterMaskIdHigh = 0x0000;
	myFilterConfig.FilterMaskIdLow = 0x0000;
	myFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
	myFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;

	if(HAL_CAN_ConfigFilter(&hcan1, &myFilterConfig) != HAL_OK)
	{
		/*Filter Configuration Eror*/
		Error_Handler();
	}
}

/*HIGHLIGHT: Redefinition of weak callback from stm32f4xx_hal_tim.c called whenever a TIMx_IT_UPDATE is read*/
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	TimeStamp += 1;
	//HAL_GPIO_TogglePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin | LED_GREEN_Pin | LED_ORANGE_Pin | LED_RED_Pin);

	/*Clear Interrupt Flags*/
	__HAL_TIM_CLEAR_IT(htim, TIM_IT_UPDATE);
	__HAL_TIM_ENABLE(htim);
}

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
	int i;
	CAN_RxHeaderTypeDef myRxMessage;
	CAN_MessageTypeDef rxMessage;
	uint8_t CANPayload[8];

	HAL_CAN_GetRxMessage(&hcan1, CAN_RX_FIFO0, &myRxMessage, CANPayload);

	if(myRxMessage.IDE)
	{
		rxMessage.CAN_Header.StdId = 0;
		rxMessage.CAN_Header.ExtId = myRxMessage.ExtId;
	}
	else
	{
		rxMessage.CAN_Header.StdId = myRxMessage.StdId;
		rxMessage.CAN_Header.ExtId = 0;
	}

	rxMessage.CAN_Header.RTR = myRxMessage.RTR;
	rxMessage.CAN_Header.IDE = myRxMessage.IDE;
	rxMessage.CAN_Header.DLC = myRxMessage.DLC;

	// TODO: Implement Error Detection and Time Stamping (Timer)
	rxMessage.TimeStamp = TimeStamp;
	rxMessage.ErrorCode = 0x00;


	for(i=0;i<(sizeof(CANPayload)/sizeof(CANPayload[0]));i++)
	{
		rxMessage.CAN_Payload[i] = CANPayload[i];
	}

	CAN_RxFIFO_Put(rxMessage);

	// FIXME: Uncomment me to blink LED
	/*PROTOCOL: ID = 0x01 & DLC = 1 & Data[0] = 0 - Turn LEDs OFF
	 * 					ID = 0x01 & DLC = 1 & Data[0] = X - Turn LEDs ON
	HAL_CAN_GetRxMessage(&hcan1, CAN_RX_FIFO0, &myRxMessage, CANPayload);
	if(myRxMessage.StdId == 0x01 && myRxMessage.DLC == 1)
	{
		if (CANPayload[0] == 0)
		{
			HAL_GPIO_WritePin(GPIOD, LED_BLUE_Pin | LED_GREEN_Pin | LED_ORANGE_Pin | LED_RED_Pin, GPIO_PIN_RESET);
		}
		else
		{
			HAL_GPIO_WritePin(GPIOD, LED_BLUE_Pin | LED_GREEN_Pin | LED_ORANGE_Pin | LED_RED_Pin, GPIO_PIN_SET);
		}
	}*/
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
