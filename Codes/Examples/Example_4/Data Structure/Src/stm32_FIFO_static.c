#include "stm32_FIFO_static.h"

void CAN_RxFIFO_Init(void)
{
    CAN_RxFIFO_GetPtr = &CAN_RxFIFO[0];
    CAN_RxFIFO_PutPtr = &CAN_RxFIFO[0];
}

uint32_t CAN_RxFIFO_Put(CAN_MessageTypeDef data)
{
    CAN_MessageTypeDef volatile *nextPutPtr;
    nextPutPtr = CAN_RxFIFO_PutPtr + 1; 
    if(nextPutPtr == &CAN_RxFIFO[CAN_FIFO_SIZE])
    { 
        nextPutPtr = &CAN_RxFIFO[0];
    }
    if(nextPutPtr == CAN_RxFIFO_GetPtr)
    { 
        /*FIFO is full*/
        return FAIL;
    } 
    else 
    { 
        *(CAN_RxFIFO_PutPtr) = data;
        CAN_RxFIFO_PutPtr = (CAN_MessageTypeDef*) nextPutPtr;
        return SUCCESS;
    }
}

uint32_t CAN_RxFIFO_Get(CAN_MessageTypeDef *dataPtr)
{
    if(CAN_RxFIFO_GetPtr == CAN_RxFIFO_PutPtr) 
    { 
        /*FIFO is empty*/
        return FAIL; 
    } 
    *dataPtr = *(CAN_RxFIFO_GetPtr++); 
    if(CAN_RxFIFO_GetPtr == &CAN_RxFIFO[CAN_FIFO_SIZE])
    { 
        CAN_RxFIFO_GetPtr = &(CAN_RxFIFO[0]);
    } 
    return SUCCESS;
}
