#include "stm32_CAN_SnifferMessage.h"

#ifndef CAN_FIFO_SIZE
#define CAN_FIFO_SIZE 10
#endif

#ifndef FAIL
#define FAIL 0
#endif

#ifndef SUCCESS
#define SUCCESS 1
#endif

CAN_MessageTypeDef CAN_RxFIFO[CAN_FIFO_SIZE];
CAN_MessageTypeDef *CAN_RxFIFO_PutPtr;
CAN_MessageTypeDef *CAN_RxFIFO_GetPtr;

void CAN_RxFIFO_Init(void);
uint32_t CAN_RxFIFO_Put(CAN_MessageTypeDef data);
uint32_t CAN_RxFIFO_Get(CAN_MessageTypeDef *dataPtr);
