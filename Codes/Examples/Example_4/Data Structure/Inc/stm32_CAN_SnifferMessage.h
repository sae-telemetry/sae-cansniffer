#include "stm32f407xx.h"
#include "stdint.h"

struct CAN_Header_Struct
{
	uint32_t StdId;
  uint32_t ExtId;
  uint32_t RTR;
  uint32_t IDE;
  uint32_t DLC;
};

struct CAN_Message_Struct
{
	uint32_t TimeStamp;
	uint32_t ErrorCode;
	uint32_t CAN_Payload[8];
	struct CAN_Header_Struct CAN_Header;
};

typedef struct CAN_Message_Struct CAN_MessageTypeDef;
typedef struct CAN_Header_Struct CAN_HeaderTypedef;
