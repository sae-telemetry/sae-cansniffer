# STM32F4DISCOVERY BOARD

## Example 1

A blinking LED example using the user button to either enable or disable blinking mode.

* A software delay is used to generate the blinking pattern.
* The user button is read in the main loop (no interruption configured).
* The user key is debounced with a software delay to ensure that a series of stable readings have been reached before the read value of the button is returned.
* A latch variable is used so that the blinking behavior is toggled each time the user button is clicked.
* _Because this example relies on something similar to a "busy wait" to produce the blink pattern, reading the switching value is closely related to the blink rate. This means that the lower the flashing frequency, the longer the time required for the user to hold the button down until the software can read its value._

## Example 2

A more advanced blink-LED example that now uses an extenal interrupt to read the user button. This alone provides a much more responsive feel for user input. In this example its possible to configure the use of NVIC block to generate the external interruption or exclusively reading the Pending Request flag inside the main loop (pooling method).

* A software delay is used to generate the blinking pattern.
* The user button is read by a _Callback_ function that is triggered either by a __NVIC__ interruption or a Pending Request flag (__EXTIT->PR__) reading in the main loop (configure the desired method by using the __NVIC_DRIVEN__ definition).
* No debounce is necessary when using the pooling method. This is because the pending request is set for the first rising edge but is only checked when the button is released and a low-level is read in the main loop.
* A latch variable is used so that the blinking behavior is toggled each time the user button is clicked.
* _In this example the blinking rate and the reading of user button are weakly related, as an interruption is beeing used in some extension. This relation is even weaker when using NVIC interruption, because the pooling method still requires a regular check of the pending request in the main loop, which holds a coupled relation with the blinking time._

## Example 3

An even more complex blink LED example. All available resources were used here to make the concurrent tasks as decoupled as possible. This means an interrupt-driven solution. An external interruption was used to process the user button and a external timer to process the blinking delay.

* A software delay is still used to generate the dead-locking period in which no other user button event is processed after a button is pressed.This helps debounce the button when using an interruption.
* The user button is read by a _Callback_ function, triggered by a __NVIC__ interruption.
* A latch variable is used so that the blinking behavior is toggled each time the user button is clicked.
* A general purpouse timer (TIM2) was used do generate the time base for the blinking LEDs. A __NVIC__ interruption is triggered when an update event is triggered. This happens when the content of the _CounterRegister_ is equal to the _Auto Reload Register_, then it is possible to know that the desired time period was elapsed, and an action must be taken.
* _Observation 1_: Note that not only the UpdateFlag is set if the content of the _CounterRegister_ corresponds to the _AutoReloadRegister_. After the first count cycle, when the _CounterRegister_ is reset to zero, it also matches the _CompareRegister_ for each one of the timer's channels.
If no configuration is made regarding the _Compare Operation Mode_, by default the timer will be configured (all registers reset to 0x00) in an _Output Compare Mode_ with 0x0000 inside the _CompareRegister_. This should not be a problem and can be ignored.
* _Observation 2_: A better approach for this specific (and simple) problem would be to use the __Output Compare__ mode, avaialble for each General Purpouse Timer on STM32. In this mode, it would be possible to activate the _Alternative Function_ for each of the LED GPIOS and use it as an output channel for the General Purpouse Timer. With this, only by setting an desired compare value to the _CompareRegister_ and a desired behavior (set output high, low or toggle), whenever the counter matches the _CompareRegister_ of a channel, the linked output would perform the action without passing trough any ISR code.

## Example 4

This is a completely different project. This code configures several modules of the STM32F4 controller as CAN, UART, TIMER etc. to enable sniffing a CAN-BUS and throwing relevant data through UART. A data structure is defined to collect all relevant information from a received CAN message. This is not 100% complete because, for example, the error detection function is not implemented and serves as the basis for future projects, especially for the configuration of all these peripheral devices.