#include <SPI.h> //Library for using SPI Communication
#include <mcp2515.h> //Library for using CAN Communication

struct can_frame canMsg;
MCP2515 mcp2515(10);

void setup(){
  while (!Serial);
  Serial.begin(9600);
  Serial.println("Serial OK");
  SPI.begin(); //Begins SPI communication
  Serial.println("SPI OK");
  
  mcp2515.reset();
  mcp2515.setBitrate(CAN_125KBPS, MCP_8MHZ); //Sets CAN at speed 125KBPS and Clock 8MHz
  mcp2515.setNormalMode();
  Serial.println("MCP OK");
  
  canMsg.can_id = 0x7FF; // Speed msgId
  canMsg.can_dlc = 8; //CAN data length as 8
  canMsg.data[0] = 0xFF; //Update temperature value in [1]
  canMsg.data[1] = 0xFF; //Update temperature value in [1]
  canMsg.data[2] = 0x00; //Rest all with 0
  canMsg.data[3] = 0x00;
  canMsg.data[4] = 0xFF;
  canMsg.data[5] = 0xFF;
  canMsg.data[6] = 0x00;
  canMsg.data[7] = 0x00;
  Serial.println("CAN MSG OK");
}

void loop(){
  mcp2515.sendMessage(&canMsg); //Sends the CAN message
  // Pela equacao da reta -> 0% perdas ate o limite de 34,4 % de carga
  //delayMicroseconds(3230); // 30% load
  //delayMicroseconds(2975); // 32,5%
  //delayMicroseconds(2740); // 35% load
  //delayMicroseconds(2560); // 37,5% load
  delayMicroseconds(2400); // 40% load
  //delayMicroseconds(2240); // 42,5% load
  //delayMicroseconds(2120); // 45% load
  //delayMicroseconds(1995); // 47,5% load
  //delayMicroseconds(1890); // 50% load
}
