#include <SPI.h> //Library for using SPI Communication
#include <mcp2515.h> //Library for using CAN Communication

struct can_frame canMsg;
MCP2515 mcp2515(10);
const int maxRPM = 65535;
const int frequency = 0.05; // [Hz]

void setup(){
  while (!Serial);
  Serial.begin(9600);
  Serial.println("Serial OK");
  SPI.begin(); //Begins SPI communication
  Serial.println("SPI OK");
  
  mcp2515.reset();
  mcp2515.setBitrate(CAN_125KBPS, MCP_8MHZ); //Sets CAN at speed 125KBPS and Clock 8MHz
  mcp2515.setNormalMode();
  Serial.println("MCP OK");
  
  canMsg.can_id = 0x02; // RPM msgId
  canMsg.can_dlc = 1; //CAN data length as 8

  canMsg.data[2] = 0x00; //Rest all with 0
  canMsg.data[3] = 0x00;
  canMsg.data[4] = 0x00;
  canMsg.data[5] = 0x00;
  canMsg.data[6] = 0x00;
  canMsg.data[7] = 0x00;
  Serial.println("CAN MSG OK");
}

void loop(){
  uint16_t rpm = maxRPM * sin(2 * PI * frequency * millis()/1000);
  canMsg.data[1] = maxRPM & (0xFF << 1) >> 1 ;
  canMsg.data[0] = maxRPM & 0xFF ;
  mcp2515.sendMessage(&canMsg); //Sends the CAN message
  delay(5);
}
