from enum import IntEnum
from quantiphy import Quantity
import numpy as np

class CANFrame:
    SOF = 0  # Start Of Frame - Single dominant bit
    STD_ID = -1  # Standard Frame ID - 11 Bit
    EXT_ID = -1  # Extended Frame ID - 29 Bit
    RTR = -1  # Remote Transmit Request- Single Bit
    SRR = -1  # Subistitute Remote Request [EXT_CAN ONLY] - Single Recessive Bit
    IDE = -1  # ID EXTENDED - Single Bit
    FDF = 0  # FD Format - Single Dominant Bit
    R0 = 0  # Reserved - Single Dominant Bit
    DLC = -1  # Data Length Code - 32-bit
    DATA = []  # Data Fied - 0 to 64 bit
    CRC = -1  # Cyclic Redundancy Check - 15 Bit
    CRC_D = 1  # CRC Delimiter - Single Recessive Bit
    ACK = 0  # Acknowledge Slot - Single Dominant Bit [Allways Ack]
    ACK_D = 1  # ACK Delimiter - Single Recessive Bit
    EOF = 0b1111111  # End Of Frame - 7 Recessive Bit


class frameModeEnum(IntEnum):
    STDID = 0
    EXTID = 1


class dataModeEnum(IntEnum):
    DATA_FRAME = 0
    REMOTE_FRAME = 1


# TODO: Define a class for operation modes (Error Active, Error Passive, STD / EXT ID, ...)
# TODO: Include DBC SUPPORT (Select Message from a list of frames, enter data directly to registered signals,...)


def getInputFrame():
    # Input General Frame Info
    # TODO: Support non-ideal operational modes
    print("\nThe input frame will be simulated as acknowledged and with no error frame")
    frameMode = int(input("\n\nSelect Frame Mode: \n0 = STDID \n1 = EXTID\n"))
    dataMode = int(input("\n\nSelect Data Mode: \n0 = DATA FRAME \n1 = REMOTE FRAME\n"))

    ID = int(input("\n\nEnter a Frame ID (HEX):\n"), 0)
    DLC = int(input("\n\nEnter Data Lenght (0-8):\n"))

    # Input Data Info
    DATA = []
    for i in range(DLC):
        dataByte = np.random.randint(0,256)
        DATA.append(dataByte)

    # Mount Frame
    inputFrame = CANFrame()
    if frameMode == frameModeEnum.STDID:
        inputFrame.IDE = 0
        inputFrame.STD_ID = ID
    else:
        inputFrame.IDE = 1
        inputFrame.SRR = 1
        inputFrame.EXT_ID = ID
    if dataMode == dataModeEnum.DATA_FRAME:
        inputFrame.RTR = 0
    else:
        inputFrame.RTR = 1

    inputFrame.DLC = DLC
    inputFrame.DATA = DATA

    return inputFrame


def printFrame(canFrame, bitStream, printAll):
    print("\n********** CAN FRAMES **********")

    if printAll == True:
        print("\nSOF:\t", canFrame.SOF)
        print("\nSTD_ID:\t", canFrame.STD_ID)
        print("\nEXT_ID:\t", canFrame.EXT_ID)
        print("\nRTR:\t", canFrame.RTR)
        print("\nSRR:\t", canFrame.SRR)
        print("\nIDE:\t", canFrame.IDE)
        print("\nFDF:\t", canFrame.FDF)
        print("\nR0:\t", canFrame.R0)
        print("\nDLC:\t", canFrame.DLC)

        for i in range(len(canFrame.DATA)):
            print("DATA[", i, "]:\t", hex(canFrame.DATA[i]))

        print("\nCRC:\t", canFrame.CRC)
        print("\nCRC_D:\t", canFrame.CRC_D)
        print("\nACK:\t", canFrame.ACK)
        print("\nACK_D:\t", canFrame.ACK_D)
        print("\nEOF:\t", canFrame.EOF)

    print("\n********** CAN BITSTREAM **********\n")
    print(bitStream.replace("", " ")[1: -1])



# ==========================================================================
# CRC Generation Unit - Linear Feedback Shift Register implementation
# (c) Kay Gorontzi, GHSi.de, distributed under the terms of LGPL
# Adapted by Luiz Bernardi
# ==========================================================================
def getCRC(rawBitStream):
    resultArray = [0 for i in range(15)]
    CRC = [0 for i in range(15)]

    for i in range(len(rawBitStream)):
        DoInvert = ("1" == rawBitStream[i]) ^ CRC[14]

        CRC[14] = CRC[13] ^ DoInvert
        CRC[13] = CRC[12]
        CRC[12] = CRC[11]
        CRC[11] = CRC[10]
        CRC[10] = CRC[9] ^ DoInvert
        CRC[9] = CRC[8]
        CRC[8] = CRC[7] ^ DoInvert
        CRC[7] = CRC[6] ^ DoInvert
        CRC[6] = CRC[5]
        CRC[5] = CRC[4]
        CRC[4] = CRC[3] ^ DoInvert
        CRC[3] = CRC[2] ^ DoInvert
        CRC[2] = CRC[1]
        CRC[1] = CRC[0]
        CRC[0] = DoInvert

    for i in range(14):
        resultArray[14 - i] = CRC[i]

    # Convert from int array to int
    resultInt = 0
    for bit in resultArray:
        resultInt = (resultInt << 1) | bit

    return resultInt


def getRawBitStream(canFrame):
    # TODO: Include verification for uninitialized (-1) fields
    rawBitStream = ""

    # SOF
    rawBitStream += format(canFrame.SOF, "01b")

    # STDID ARBITRATION
    if canFrame.IDE == frameModeEnum.STDID:
        rawBitStream += format(canFrame.STD_ID, "011b")
        rawBitStream += format(canFrame.RTR, "01b")
        rawBitStream += format(canFrame.IDE, "01b")
        rawBitStream += format(canFrame.FDF, "01b")
    # EXTID ARBITRATION
    else:
        upID = canFrame.EXT_ID & 0x1FFC0000
        lowID = canFrame.EXT_ID & 0x03FFFF
        rawBitStream += format(upID, "011b")
        rawBitStream += format(canFrame.SRR, "01b")
        rawBitStream += format(canFrame.IDE, "01b")
        rawBitStream += format(lowID, "018b")
        rawBitStream += format(canFrame.RTR, "01b")
        rawBitStream += format(canFrame.R0, "01b")

    # DATA
    rawBitStream += format(canFrame.DLC, "04b")
    for i in range(len(canFrame.DATA)):
        rawBitStream += format(canFrame.DATA[i], "08b")

    # CRC
    canFrame.CRC = getCRC(rawBitStream)
    rawBitStream += format(canFrame.CRC, "015b")
    rawBitStream += format(canFrame.CRC_D, "01b")

    # ACK
    rawBitStream += format(canFrame.ACK, "01b")
    rawBitStream += format(canFrame.ACK_D, "01b")

    # EOF
    rawBitStream += format(canFrame.EOF, "07b")

    return rawBitStream


def getStuffFrame(rawFrame):
    noStuffBits = 10  # EOF + ACK + CDC_D
    startStuffBit = 4

    stuffedFrame = list(rawFrame)
    i = startStuffBit
    stopCondition = len(stuffedFrame) - noStuffBits

    while i < stopCondition:
        if (
            stuffedFrame[i]
            == stuffedFrame[i - 1]
            == stuffedFrame[i - 2]
            == stuffedFrame[i - 3]
            == stuffedFrame[i - 4]
        ):
            # FIXME: Find a better solution for type conversion
            stuffedFrame.insert(i + 1, str(int(not (int(stuffedFrame[i])))))
            stopCondition += 1
        i += 1

    stuffedFrame = "".join(stuffedFrame)

    return stuffedFrame


busSpeed = int(input("\n\nSet Bus Speed (baud/s):\n"))
inputFrame = getInputFrame()

rawFrame = getRawBitStream(inputFrame)
bitStream = getStuffFrame(rawFrame)

printFrame(inputFrame, bitStream, True)
print("\nMessage Time =\t",Quantity(len(bitStream)/busSpeed)," sec")
