import argparse

def hex2bin(hex_file, output_file):
    save_output = output_file is not None

    with open(hex_file, 'r') as file:
        input_data = file.read().replace('\n', '')

    bin_data = bytes.fromhex(input_data)

    if save_output:
        resultFile = open(output_file, "wb")
        resultFile.write(bin_data)
    else:
        print(bin_data)


def main():
    desc_str = """Translate HEX files to Binary files"""

    output_file_help = 'output filename, if not given the processed data will be printed to stdout'
    hex_file_help = 'HEX input file (i.e: from UART Sniffer)'

    parser = argparse.ArgumentParser(description=desc_str)
    parser.add_argument('--hex-file', dest='hex_file', required=True, help=hex_file_help)
    parser.add_argument('--output', dest='output_file', required=False, help=output_file_help)

    args = parser.parse_args()

    hex2bin(args.hex_file, args.output_file)

    return

if __name__ == "__main__":
    main()
