import numpy as np
import math
import zlib


# Second fraction = ( PREDIV_S - SS ) / ( PREDIV_S + 1 ) --> prediv_s = 255


class rtcClass:
    year: int
    month: int
    date: int
    hour: int
    minute: int
    seconds: int
    subSeconds: int


class messageClass:
    stdId: int
    extId: int
    rtr: int
    ide: int
    dlc: int

    seqNumber = 1
    CAN_Payload = list()
    errorCode: int

# FILE CONFIG
resultFile = open("known-can-sequence-bin.txt", "wb")

# RTC CONFIG - Check parsed file to match first msg rtc data
rtc = rtcClass()
rtc.year = 21
rtc.month = 2
rtc.date = 1
rtc.hour = 19
rtc.minute = 45
rtc.seconds = 3
rtc.subSeconds = 243  # (255-178)/(255+1) = x secs

# MESSAGE CONFIG
message = messageClass()
message.ide = 0  # StdId test only
message.extId = 0  # StdId test only
message.rtr = 0  # Data frame test only
message.errorCode = 0  # Valid messages test only

# DELAY CONFIG - Check known-can-sequence to match delay
delay = 0.01
time = (255 - rtc.subSeconds) / (255 + 1)

numMessages = 0

for id in range(1, 16):
    message.stdId = id
    for dlc in range(1, 9):
        message.dlc = dlc
        for payload in range(0, 255):
            if dlc == 1:
                message.CAN_Payload = [payload, 0, 0, 0, 0, 0, 0, 0]
            elif dlc == 2:
                message.CAN_Payload = [payload, payload, 0, 0, 0, 0, 0, 0]
            elif dlc == 3:
                message.CAN_Payload = [payload, payload, payload, 0, 0, 0, 0, 0]
            elif dlc == 4:
                message.CAN_Payload = [payload, payload, payload, payload, 0, 0, 0, 0]
            elif dlc == 5:
                message.CAN_Payload = [
                    payload,
                    payload,
                    payload,
                    payload,
                    payload,
                    0,
                    0,
                    0,
                ]
            elif dlc == 6:
                message.CAN_Payload = [
                    payload,
                    payload,
                    payload,
                    payload,
                    payload,
                    payload,
                    0,
                    0,
                ]
            elif dlc == 7:
                message.CAN_Payload = [
                    payload,
                    payload,
                    payload,
                    payload,
                    payload,
                    payload,
                    payload,
                    0,
                ]
            else:
                message.CAN_Payload = [
                    payload,
                    payload,
                    payload,
                    payload,
                    payload,
                    payload,
                    payload,
                    payload,
                ]

            messageStream = (
                message.seqNumber.to_bytes(4,"little")
                + rtc.date.to_bytes(4, "little")
                + rtc.month.to_bytes(4, "little")
                + rtc.year.to_bytes(4, "little")
                + rtc.hour.to_bytes(4, "little")
                + rtc.minute.to_bytes(4, "little")
                + rtc.seconds.to_bytes(4, "little")
                + rtc.subSeconds.to_bytes(4, "little")
                + message.stdId.to_bytes(4, "little")
                + message.extId.to_bytes(4, "little")
                + message.rtr.to_bytes(4, "little")
                + message.ide.to_bytes(4, "little")
                + message.dlc.to_bytes(4, "little")
                + message.CAN_Payload[0].to_bytes(4, "little")
                + message.CAN_Payload[1].to_bytes(4, "little")
                + message.CAN_Payload[2].to_bytes(4, "little")
                + message.CAN_Payload[3].to_bytes(4, "little")
                + message.CAN_Payload[4].to_bytes(4, "little")
                + message.CAN_Payload[5].to_bytes(4, "little")
                + message.CAN_Payload[6].to_bytes(4, "little")
                + message.CAN_Payload[7].to_bytes(4, "little")
                + message.errorCode.to_bytes(4, "little")
            )

            crc = zlib.crc32(messageStream)
            messageStream = messageStream + crc.to_bytes(4, "little")

            numMessages+=1

            resultFile.write(messageStream)
            message.seqNumber+=1
            time = round(time + delay, 6)
            #time += 0.006303

            # RTC UPDATE
            rtc.subSeconds = math.floor(
                abs(
                    (255 * math.floor(time))
                    - abs(math.floor(time) + 255 - time * (255 + 1))
                )
            )
            #TODO: change magical numbers to input arguments (initial seconds, etc...)
            rtc.seconds = math.floor((3 + math.floor(time)) % 60)
            rtc.minute = math.floor((45 + math.floor(time / 60)) % 60)
            rtc.hour = math.floor((19 + math.floor(time / 3600)) % 24)

print("Execution Completed")