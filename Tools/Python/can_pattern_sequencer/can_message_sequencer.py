import cantools
import math
import argparse
from pyusbtin.usbtin import USBtin
from pyusbtin.canmessage import CANMessage
import time
from random import seed
from random import random


seed(1)

def now():
    return int(time.time()*1000)

# start time in ms
start_time = now()


def message_ids(ids):
    return [int(id) for id in ids.split(',')]

def payload_generator(message, function_type, period):
    global start_time
    function = lambda signal: signal.minimum + (random() * (signal.maximum - signal.minimum))

    if function_type == 'sin':
        function = lambda signal: signal.minimum + ((math.sin(2*math.pi*(now() - start_time)/period) + 1)/2 * (signal.maximum - signal.minimum))
    elif function_type == 'cos':
        function = lambda signal: signal.minimum + ((math.cos(2*math.pi*(now() - start_time)/period) + 1)/2 * (signal.maximum - signal.minimum))

    while True:
        data = {}
        for signal in message.signals:
            data[signal.name] = function(signal)
        print(f'Message {message.frame_id} sent')
        yield (message.frame_id, list(message.encode(data, strict=True)))


def send_messages(port, dbc_file, interval, filter_ids, quantity, function, period):
    usbtin = USBtin()
    usbtin.connect(port)
    usbtin.open_can_channel(125000, usbtin.ACTIVE)

    dbc = cantools.database.load_file(dbc_file)

    messages = dbc.messages
    if filter_ids != None:
        messages = list(filter(lambda message: message.frame_id in filter_ids, messages))

    msg_generators = [payload_generator(message, function, period) for message in messages]

    msg_count = 0
    while msg_count < quantity:
        print(f'Message count: {msg_count}')
        for msg_generator in msg_generators:
            can_message = CANMessage(*msg_generator.__next__())
            usbtin.send(can_message)
            time.sleep(interval/1000)
            msg_count = msg_count + 1

    usbtin.close_can_channel()
    print("Execution Completed")
    return


def main():
    desc_str = """Send CAN messages"""

    parser = argparse.ArgumentParser(description=desc_str)

    dbc_file_help = 'DBC file used to generate messages'
    parser.add_argument('--dbc-file', '-d', dest='dbc_file', required=True, help=dbc_file_help)

    msg_interval_help = 'Time interval between messages in ms'
    parser.add_argument('--msg-int', '-i', dest='msg_interval', required=False,
                        help=msg_interval_help, type=int, default=1000)

    port_help = 'Host port to connect and send data'
    parser.add_argument('--port', '-p', dest='port', required=True, help=port_help)

    filter_help = 'Frame IDs to send messages, if not provided all frame IDs will be used'
    parser.add_argument('--filter', '-f', dest='filter', required=False,
                        help=filter_help, type=message_ids)

    msg_quant_help = 'Number of messages sent per frame ID'
    parser.add_argument('--quantity', '-q', dest='quantity', required=False,
                        help=msg_quant_help, type=int, default=10)

    function_help = 'Function to generate data, default is random'
    parser.add_argument('--function', '-F', dest='function', required=False,
                        help=function_help, default='random')

    period_help = 'Period to use in function in ms'
    parser.add_argument('--period', '-T', dest='period', required=False,
                        help=period_help, type=int)

    args = parser.parse_args()

    send_messages(args.port, args.dbc_file, args.msg_interval, args.filter, args.quantity, args.function, args.period)

    return

if __name__ == "__main__":
    main()
