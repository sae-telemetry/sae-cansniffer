from pyusbtin.usbtin import USBtin
from pyusbtin.canmessage import CANMessage
import time

usbtin = USBtin()
usbtin.connect("COM3")
usbtin.open_can_channel(250000, usbtin.ACTIVE)

#defaultMessages = [
#    CANMessage(0x01, [0x01]),
#    CANMessage(0x02, [0x22]),
#    CANMessage(0x03, [0x33]),
#    CANMessage(0x04, [0x44]),
#    CANMessage(0x05, [0x55]),
#    CANMessage(0x06, [0x66]),
#    CANMessage(0x07, [0x77]),
#    CANMessage(0x08, [0x88]),
#    CANMessage(0x09, [0x99]),
#    CANMessage(0x10, [0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08]),
#    CANMessage(0x1A, [0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA]),
#    CANMessage(0x15, [0x55,0x55,0x55,0x55,0x55,0x55,0x55,0x55]),
#    CANMessage(0x3F, [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF]),
#    CANMessage(0x01, [0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00]),
#    CANMessage(0x0A, [0x5A,0x5A,0x5A,0x5A,0x5A,0x5A,0x5A,0x5A]),
#    CANMessage(0x01, [0x01,0x02,0x03]),
#    CANMessage(0x01, [0x01,0x02,0x03,0x04]),
#    CANMessage(0x01, [0x01,0x02,0x03,0x04, 0x05]),
#    CANMessage(0x01, [0x01,0x02]),
#    CANMessage(0x01, [0x00])    
#]

for id in range(1,16):
    for dlc in range(1,9):
        for payload in range (0,255):
            if dlc == 1:
                message = CANMessage(id, [payload])
            elif dlc == 2:
                message = CANMessage(id, [payload,payload])
            elif dlc == 3:
                message = CANMessage(id, [payload,payload,payload])
            elif dlc == 4:
                message = CANMessage(id, [payload,payload,payload,payload])
            elif dlc == 5:
                message = CANMessage(id, [payload,payload,payload,payload,payload])
            elif dlc == 6:
                message = CANMessage(id, [payload,payload,payload,payload,payload,payload])
            elif dlc == 7:
                message = CANMessage(id, [payload,payload,payload,payload,payload,payload,payload])
            else:
                message = CANMessage(id, [payload,payload,payload,payload,payload,payload,payload,payload])
            usbtin.send(message)
            print("Message Sent - ID:",id," DLC: ", dlc, " PAYLOAD: ", payload)
            time.sleep(0.01)

print("Execution Completed")

usbtin.close_can_channel()
