# IMPORT SYSTEM PROPRIETIES
from os import system, name 
import threading
import time
# IMPORT MATH LIBS
import math
import numpy as np
import matplotlib.pyplot as plt
# IMPORT DATA PRETTY PRINTER
from pprint import pprint
# IMPORT USBTIN ELEMENTS
from pyusbtin.usbtin import USBtin
from pyusbtin.canmessage import CANMessage

class CANSignalConfiguration:
    def __init__(self,CANMsg,SignalName,signalFreq_Hz,signalSamplesPerCycle,signalAmplitude_bytes,signalIsSigned):
        self.CANMsg = CANMsg
        self.SignalName = SignalName
        self.signalFreq_Hz = signalFreq_Hz
        self.signalAmplitude_bytes = signalAmplitude_bytes
        self.signalSamplesPerCycle = signalSamplesPerCycle
        self.signalIsSigned = signalIsSigned

def SineGenerator (frequency,amplitude,samplesPerCycle,signalIsSigned):
    signalTime = np.arange(0,1/frequency,(1/frequency)/samplesPerCycle)
    trigValue = np.sin(2*math.pi*frequency*signalTime)
    aboveZero = (trigValue > 0) * 1
    signalAmplitude = (amplitude/2) * trigValue + ((np.logical_and(aboveZero,1)) * -1) * signalIsSigned + (np.logical_not(signalIsSigned) * amplitude/2)
    return signalTime,signalAmplitude

class SignalGenerator(threading.Thread):
    def __init__(self, threadID, name, counter, CANSignalConfiguration):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.counter = counter
        self.configuration = CANSignalConfiguration
        self.status = threading.Event()

    def run(self):
        print ("Starting " + self.configuration.SignalName + " signal generator")
        
        # BUILD SIGNAL GENERATOR - DEFINE SAMPLE FREQ HERE - BE AWARE OF FIFO SIZE
        threadDelay = (1 / self.configuration.signalFreq_Hz) / self.configuration.signalSamplesPerCycle

        signalTime,signalAmplitude = SineGenerator(self.configuration.signalFreq_Hz,\
            math.pow(2,8*self.configuration.signalAmplitude_bytes),self.configuration.signalSamplesPerCycle,\
                self.configuration.signalIsSigned)
        
        while(not self.status.is_set()):
            i = 0
            while i < len(signalTime):
                self.configuration.CANMsg.__setattr__(self.configuration.SignalName,signalAmplitude[i])
                threadLock.acquire()
                usbtin.send(self.configuration.CANMsg)
                threadLock.release()
                i += 1
                time.sleep(threadDelay)
        print ("Exiting " + self.name)
    
threadLock = threading.Lock()
threads = []

# LOAD DBC FILE AND CONFIGURE MESSAGES
CANMessage.load_dbc(r'FormulaUTFPR_CAN.dbc')

Acc01 = CANMessage(0x401)
Acc02 = CANMessage(0x402)
Acc03 = CANMessage(0x403)
AirTemp = CANMessage(0x201)
GearPosition = CANMessage(0x202)
Lambda = CANMessage(0x101)
MAP = CANMessage(0x102)
OilPressure = CANMessage(0x1)
OilTemp = CANMessage(0x203)     # NO SIGNAL DEFINED
Pressure = CANMessage(0x204)
RPM = CANMessage(0x2)
Speed = CANMessage(0x301)
SuspFront = CANMessage(0x404)
SuspRear = CANMessage(0x405)
Temperature = CANMessage(0x205) # NO SIGNAL DEFINED
TPS = CANMessage(0x103)
WaterTemp = CANMessage(0x206)

# CONFIGURE USBTIN DEVICE
usbtin = USBtin()
usbtin.connect("COM3")
usbtin.open_can_channel(250000, USBtin.ACTIVE)

# CREATE THREADS
configGenerator1 = CANSignalConfiguration(Speed,'Speed',10,10,2,False) # Msg:SPEED, Signal:Speed, 10 Hz, 10 samples, 2 bytes, Unsigned
configGenerator2 = CANSignalConfiguration(RPM,'RPM',100,10,1,True)     # Msg:RPM, Signal:RPM, 100 Hz, 10 samples, 1 bytes, Signed
thread1 = SignalGenerator(1, "SignalGenerator 1", 1, configGenerator1)
thread2 = SignalGenerator(2, "SignalGenerator 2", 2, configGenerator2)

# START THREADS
thread1.start()
thread2.start()

# Add threads to thread list
threads.append(thread1)
threads.append(thread2)

# Wait for all threads to complete
userInput = ''
while(userInput != 'c'):
    print("Press 'c' to cancel programm execution")
    userInput = input()

print ("Closing Open Threads")
for t in threads:
    t.status.set()

print ("Closing Main Threads")