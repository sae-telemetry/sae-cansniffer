import argparse
import json
import os
import struct
import zlib

MESSAGE_STRUCT = [
    # SeqNumber
    ('Sequence Number', 0),

    # Timestamp
    ('Date', 1),
    ('Month', 2),
    ('Year', 3),
    ('Hours', 4),
    ('Minutes', 5),
	('Seconds',6),
	('SubSeconds', 7),

    # CAN
	('StdId', 8),
	('ExtId', 9),
	('RTR', 10),
	('IDE', 11),
	('DLC', 12),
	('CAN_Payload', (13,20)),

    # Error Info
    ('ErrorCode', 21),

    # CRC
    ('CRC', 22)
]

# Message size in bytes
MSG_SIZE_WITHOUT_CRC = 88
MESSAGE_SIZE = 92

def bin2can(bin_file, output):
    save_output = output is not None

    bin_file_handler = open(bin_file, 'rb')

    bin_file_size = os.path.getsize(bin_file)

    for msgidx in range(0, int(bin_file_size/MESSAGE_SIZE)):
        message_bytes = bin_file_handler.read(MESSAGE_SIZE)

        message_bytes_without_crc = message_bytes[:MSG_SIZE_WITHOUT_CRC]
        crc = zlib.crc32(message_bytes_without_crc)

        parsed_message = struct.unpack(int(MESSAGE_SIZE/4)*'I', message_bytes)
        json_message = {}
        for (name, field_range) in MESSAGE_STRUCT:
            if type(field_range) == tuple:
                (start, end) = field_range
                json_message[name] = [item for item in parsed_message[start:end+1]]
                for idx in range(0, len(json_message[name])):
                    json_message[name][idx] = hex(json_message[name][idx])
            else:
                json_message[name] = hex(parsed_message[field_range])


        json_message_str = json.dumps(json_message, indent=4)

        if save_output:
            with open(output, 'a') as output_file:
                output_file.write(f'\nMessage {msgidx+1}\n')
                if hex(crc) != json_message['CRC']:
                    output_file.write('\x1b[6;37;41m' + f'CRC check failed for message {msgidx+1}' + '\x1b[0m' + '\n')
                output_file.write(json_message_str + '\n')
        else:
            print(f'Message {msgidx+1}')
            if hex(crc) != json_message['CRC']:
                print('\x1b[6;37;41m' + f'CRC check failed for message {msgidx+1}' + '\x1b[0m')
            print(json_message_str)

    return


def main():
    desc_str = """Translate binary files in CAN messages"""

    output_help = 'output filename, if not given the data will only be printed to stdout'
    bin_file_help = 'Binary file holding the messages'

    parser = argparse.ArgumentParser(description=desc_str)
    parser.add_argument('--bin-file', dest='bin_file', required=True, help=bin_file_help)
    parser.add_argument('--output', dest='output', required=False, help=output_help)
    #FIXME: DEBUG
    #args = parser.parse_args(['--bin-file', '20210104.TXT'])

    args = parser.parse_args()

    bin2can(args.bin_file, args.output)

    return

if __name__ == "__main__":
    main()
